# Ansible

### Introduction to Ansible
- What is ansible
- How does ansible work

### YAML
- YAML syntax

### Ansible concepts
- Inventories
- Variables
- Playbooks
- Roles

read over all Ansible official docs, except:
- become and windows
- migrating from with x to loops
- adding controls to loops
- testing truthiness
- working with language specific version managers
- playbook tags
- working with dynamic inventory
- connection methods and details
- command line tools
- interactive input prompts

